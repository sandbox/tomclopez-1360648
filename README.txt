The Node Revision Reference URL Widget module adds a new widget to the
Node Revision Reference field type. It auto-populates a node revision 
reference field with a value from the URL, and wont not allow this value to be changed once set.

Support
-------
If you experience a problem with this module or have a problem, file a
request or issue in the queue at http://drupal.org/sandbox/TomCLopez/1360648
DO NOT POST IN THE FORUMS. Posting in the issue queues is a direct line of 
communication with the module
authors.
